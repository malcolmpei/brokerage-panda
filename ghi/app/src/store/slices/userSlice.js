import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        user: {
            name: '',
            username: '',
            profile_image: ''
        },
        loginModal: {
            visible: false
        },
        createAccountModal: {
            visible: false
        },
        backgroundBlur: {
            inEffect: false
        },
        symbolLookUpResult: {
            visible: false,
            data: ['no data']
        },
        quoteResult: {},
        windowWidth: 900
    },
    reducers: {
        setUser: (state, action) => {
            state.user = action.payload
        },
        setLoginModal: (state, action) => {
            state.loginModal.visible = action.payload
        },
        setCreateAccountModal: (state, action) => {
            state.createAccountModal.visible = action.payload
        },
        setBackgroundBlur: (state, action) => {
            state.backgroundBlur.inEffect = action.payload
        },
        setSymbolLookUpResult: (state, action) => {
            state.symbolLookUpResult = action.payload
        },
        setQuoteResult: (state, action) => {
            state.quoteResult = action.payload
        },
        setWindowWidth: (state, action) => {
            state.windowWidth = action.payload
        }
    }
})

export const {
    setUser,
    setLoginModal,
    setCreateAccountModal,
    setBackgroundBlur,
    setSymbolLookUpResult,
    setQuoteResult,
    setWindowWidth
} = userSlice.actions
export default userSlice.reducer
