import { createSlice } from "@reduxjs/toolkit";

export const navbarSlice = createSlice({
    name: 'navbar',
    initialState: {
        services: {
            visible: false
        },
        docs: {
            visible: false
        },
        company: {
            visible: false
        },
        navModal: {
            visible: false
        }
    },
    reducers: {
        setServices: (state, action) => {
            state.services.visible = action.payload
        },
        setDocs: (state, action) => {
            state.docs.visible = action.payload
        },
        setCompany: (state, action) => {
            state.company.visible = action.payload
        },
        setNavModal: (state, action) => {
            state.navModal.visible = action.payload
        }
    }
})

export const {
    setServices,
    setDocs,
    setCompany,
    setNavModal
} = navbarSlice.actions
export default navbarSlice.reducer
