import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { authApi } from './authApi'

export const finnhubApi = createApi({
    reducerPath: 'finnhubApi',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_BROKERAGE_API_HOST,
        prepareHeaders: (headers, {getState}) => {
            const selector = authApi.endpoints.getToken.select()
            const {data: tokenData} = selector(getState())
            if (tokenData) {
                headers.set('Authorization', `Beaer ${tokenData.access_token}`)
            }
            return headers
        }
    }),
    tagTypes: ['News'],
    endpoints: build => ({
        getMarketNews: build.query({
            query: () => ({
                url: '/api/finnhub/news',
                method: 'get',
                credentials: 'include'
            }),
            providesTags: ['News']
        }),
        symbolLookup: build.mutation({
            query: data => ({
                url: `/api/finnhub/symbols?query=${data}`,
                method: 'get'
            })
        }),
        getQuote: build.mutation({
            query: data => ({
                url: '/api/finnhub/quote',
                method: 'get',
                credentials: 'include',
                body: data
            })
        }),
        getIpoCalendar: build.mutation({
            query: data => ({
                url: '/api/finnhub/ipo/calendar',
                method: 'post',
                credentials: 'include',
                body: data
            })
        })
    })
})

export const {
    useGetMarketNewsQuery,
    useSymbolLookupMutation
} = finnhubApi
