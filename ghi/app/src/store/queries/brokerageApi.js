import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { authApi } from './authApi'

export const brokerageApi = createApi({
    reducerPath: 'brokerageApi',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_BROKERAGE_API_HOST,
        prepareHeaders: (headers, {getState}) => {
            const selector = authApi.endpoints.getToken.select()
            const {data: tokenData} = selector(getState())
            if (tokenData) {
                headers.set('Authorization', `Bearer ${tokenData.access_token}`)
            }
            return headers
        }
    }),
    tagTypes: ['Stocks'],
    endpoints: build => ({
        getStocks: build.mutation({
            query: () => ({
                url: '/api/stocks',
                method: 'get',
                credentials: 'include'
            }),
            providesTags: ['Stocks']
        }),
        saveStock: build.mutation({
            query: data => ({
                url: '/api/stocks',
                method: 'post',
                credentials: 'include',
                body: data
            })
        })
    })
})
export const {
    useGetStocksMutation,
    useSaveStockMutation
} = brokerageApi
