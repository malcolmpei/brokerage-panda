import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const authApi = createApi({
    reducerPath: 'authApi',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_BROKERAGE_API_HOST,
        prepareHeaders: (headers, {getState}) => {
            const selector = authApi.endpoints.getToken.select()
            const {data: tokenData} = selector(getState())
            if (tokenData) {
                headers.set('Authorization', `Bearer ${tokenData.access_token}`)
            }
            return headers
        }
    }),
    tagTypes: ['Token', 'User'],
    endpoints: build => ({
        getToken: build.query({
            query: () => ({
                url: '/token',
                method: 'get',
                credentials: 'include'
            }),
            providesTags: ['Token']
        }),

        getUserByUsername: build.mutation({
            query: data => ({
                url: `/api/user?username=${data}`,
                method: 'get',
                credentials: 'include'
            }),
            providesTags: ['User']
        }),

        logIn: build.mutation({
            query: data => {
                let formData = null
                if (data instanceof HTMLElement) {
                    formData = new FormData(data)
                } else {
                    formData = new FormData()
                    formData.append('username', data.username)
                    formData.append('password', data.password)
                }
                return {
                    url: '/token',
                    method: 'post',
                    credentials: 'include',
                    body: formData
                }
            },
            invalidatesTags: ['Token', 'User']
        }),

        logOut: build.mutation({
            query: () => ({
                url: '/token',
                method: 'delete',
                credentials: 'include'
            }),
            invalidatesTags: ['Token', 'User']
        }),

        createUser: build.mutation({
            query: data => {
                return {
                    url: '/api/user',
                    method: 'post',
                    credentials: 'include',
                    body: data
                }
            },
            providesTags: ['Token', 'User']
        }),

        updateUser: build.mutation({
            query: data => {
                return {
                    url: '/api/user',
                    method: 'put',
                    credentials: 'include',
                    body: data
                }
            },
            invalidatesTags: ['Token', 'User']
        }),

        deleteUser: build.mutation({
            query: () => {
                return {
                    url: '/api/user',
                    method: 'delete',
                    credentials: 'include'
                }
            },
            invalidatesTags: ['Token', 'User']
        })
    })
})

export const {
    useGetTokenQuery,
    useLogInMutation,
    useLogOutMutation,
    useCreateUserMutation,
    useUpdateUserMutation,
    useDeleteUserMutation,
    useGetUserByUsernameMutation
} = authApi
