import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { authApi } from './authApi'

export const polygonApi = createApi({
    reducerPath: 'polygonApi',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_BROKERAGE_API_HOST,
        prepareHeaders: (headers, {getState}) => {
            const selector = authApi.endpoints.getToken.select()
            const {data: tokenData} = selector(getState())
            if (tokenData) {
                headers.set('Authorization', `Bearer ${tokenData.access_token}`)
            }
            return headers
        }
    }),
    tagTypes: ['Tickers'],
    endpoints: build => ({
        getTickerList: build.mutation({
            query: data => ({
                url: `/api/polygon/tickers?limit=${data}`,
                method: 'get',
                credentials: 'include'
            })
        }),
        getTickerDetails: build.mutation({
            query: data => ({
                url: `/api/polygon/ticker?symbol=${data}`,
                method: 'get',
                credentials: 'include'
            })
        })
    })
})

export const {
    useGetTickerListMutation,
    useGetTickerDetailsMutation
} = polygonApi
