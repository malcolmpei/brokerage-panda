import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/dist/query'

import { authApi } from './queries/authApi'
import { finnhubApi } from './queries/finnhubApi'
import { polygonApi } from './queries/polygonApi'

import { userSlice } from './slices/userSlice'
import { navbarSlice } from './slices/navbarSlice'


export const store = configureStore({
    reducer: {
        [ authApi.reducerPath ] : authApi.reducer,
        [ finnhubApi.reducerPath ]: finnhubApi.reducer,
        [ polygonApi.reducerPath ]: polygonApi.reducer,
        user : userSlice.reducer,
        navbar : navbarSlice.reducer
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware()
            .concat(authApi.middleware)
            .concat(finnhubApi.middleware)
            .concat(polygonApi.middleware)
    }
})

setupListeners(store.dispatch)
