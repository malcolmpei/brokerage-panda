import queryIcon from '../images/query-icon.svg'
import stockIcon from '../images/stocks-icon.svg'
import newsIcon from '../images/news-icon.svg'
import indicesIcon from '../images/indices-icon.svg'

import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { setServices } from '../store/slices/navbarSlice'

export default function ServicesModal(){
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const handleNewsButton = () => {
        navigate('/finnhub/news')
        dispatch(setServices(false))
    }
    const handleStocksButton = () => {
        navigate('/polygon/stocks')
        dispatch(setServices(false))
    }

    return (
        <div id="services-modal">
            <div className="nav-menu-title">Services</div>
            <button onClick={handleStocksButton} className='nav-buttons'><img alt='' src={stockIcon} className='icons'/>- Stocks</button>
            <button className='nav-buttons'><img alt='' src={indicesIcon} className='icons'/>- Indices</button>
            <button className='nav-buttons'><img alt='' src={queryIcon} className='icons'/>- Symbol Lookup</button>
            <button onClick={handleNewsButton} className='nav-buttons'><img alt='' src={newsIcon} className='icons'/>- Market News</button>
        </div>
    )
}
