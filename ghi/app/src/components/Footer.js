

export default function Footer() {

    return (
        <div id="footer">
            <div className="footer-logo">Brokepanda.io</div>
            <div>
                <div className="footer-titles">Services</div>
                <button className="nav-buttons">Stocks</button>
                <button className="nav-buttons">Indices</button>
                <button className="nav-buttons">Aggregates</button>
            </div>
            <div>
                <div className="footer-titles">Documentation</div>
            </div>
            <div>
                <div className="footer-titles">Company</div>
            </div>
            <div>
                <div className="footer-titles">Legal</div>
            </div>
        </div>
    )
}
