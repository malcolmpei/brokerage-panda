import { useSelector } from "react-redux"
import { useGetTokenQuery } from "../store/queries/authApi"
import { useNavigate } from "react-router-dom"


export default function NavMenuModal({
    handleLogoutButton,
    handleLogin,
    handleSignup,
    handlePricingButton,
    handleContactButton
}) {
    const navigate = useNavigate()
    const token = useGetTokenQuery()

    const userSliceData = useSelector(state => state.user)
    return (
        <div id="nav-menu-modal">
            <button className="nav-buttons">Services</button>
            <button className="nav-buttons">Docs</button>
            <button className="nav-buttons">Company</button>
            <button onClick={handlePricingButton} className="nav-buttons">Pricing</button>
            <button onClick={handleContactButton} className="nav-buttons">Contact</button>
            {token.data
            ?   (
                <div>
                    <button onClick={()=> {navigate('/profile')}} className="nav-buttons">Profile</button>
                    <button onClick={handleLogoutButton} className="nav-buttons">Log out</button>
                </div>
                )
            :   (
                <div>
                    <button onClick={handleLogin} className="nav-buttons">Log in</button>
                    <button onClick={handleSignup} className="nav-buttons">Sign Up</button>
                </div>
                )
            }
        </div>
    )
}
