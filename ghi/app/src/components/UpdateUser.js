import { useNavigate } from "react-router-dom"
import { useGetTokenQuery, useLogOutMutation, useUpdateUserMutation } from "../store/queries/authApi"
import { useState } from "react"


export default function UpdateUser() {
    const navigate = useNavigate()

    const [updateUser] = useUpdateUserMutation()
    const [logout] = useLogOutMutation()

    const token = useGetTokenQuery()

    const [formData, setFormData] = useState({
        name: '',
        username: '',
        profile_image: ''
    })

    const handleFormChange = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.currentTarget.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        if (token.isSuccess) {
            if (formData.name === ''){
                formData.name = token.currentData.user.name
            }
            if (formData.username === '') {
                formData.username = token.currentData.user.username
            }
            if (formData.profile_image === '') {
                formData.profile_image = token.currentData.user.profile_image
            }
            const result = await updateUser(formData)
            if (result.data) {
                logout()
                navigate('/')
            }
        }

    }
    return (
        <div id="update-account-container">
            <div id="update-account-card">
                <h1>update user</h1>
                <form className="account-forms" onSubmit={handleSubmit}>
                    <label>Username</label>
                    <input onChange={handleFormChange} className="input" placeholder={token.isSuccess ? token.currentData.user.username : 'Username'} name="username"/>
                    <label>Full name</label>
                    <input onChange={handleFormChange} className="input" placeholder={token.isSuccess ? token.currentData.user.name : "Full Name"} name="name"/>
                    <label>Profile Image</label>
                    <input onChange={handleFormChange} className="input" placeholder={token.isSuccess ? token.currentData.user.profile_image : "Profile Image URL"} name="profile_image"/>
                    <button className="submitButton">submit</button>
                </form>
            </div>
        </div>
    )
}
