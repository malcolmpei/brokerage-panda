import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import closeIcon from '../images/close-icon.svg'
import { setSymbolLookUpResult } from "../store/slices/userSlice"

export default function SymbolSearchResult() {
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const symbolSearchResults = useSelector(state => state.user.symbolLookUpResult)

    const handleSymbolButton = event => {
        let symbol = event.currentTarget.innerText.split(" ")[1].split(".")[0]

    }
    const handleCloseButton = () => {
        dispatch(setSymbolLookUpResult({
            visible: false,
            data: ['no data']
        }))
    }

    if (symbolSearchResults.data.length === 1){
        return (
            <div id="symbol-search-results">
                <div id="symbol-search-container">
                    <div className="nav-menu-title">Search Results</div>
                    <div>Failed to retrieve data</div>
                    <button onClick={handleCloseButton} className="symbol-result-close"><img alt='' src={closeIcon} className="icons"/></button>
                </div>
            </div>
        )
    } else {
        return(
            <div id="symbol-search-results">
                <div id="symbol-search-container">
                    <div className="nav-menu-title">Search Results</div>
                    <div id="symbol-search-results-container">
                        {symbolSearchResults.data.map(result => {
                            return (
                                <div onClick={handleSymbolButton} value={result.symbol} key={result.symbol} className="symbol-card">
                                    <div className="result-description">{result.description}</div>
                                    <div className="result-symbol"><span className="tag">symbol:</span> {result.symbol}</div>
                                </div>
                            )
                        })}
                    </div>
                    <button onClick={handleCloseButton} className="symbol-result-close"><img src={closeIcon} className="icons"/></button>
                </div>
            </div>
        )
    }
}
