import contactIcon from '../images/contact-icon.svg'
import infoIcon from '../images/info-icon.svg'


export default function CompanyModal() {

    return (
        <div id="company-modal">
            <div className="nav-menu-title">Company Information</div>
            <button className="nav-buttons"><img alt='' src={contactIcon} className='icons' />- Careers</button>
            <button className="nav-buttons"><img alt='' src={infoIcon} className='icons'/>- About Us</button>
        </div>
    )
}
