import { useState } from "react"
import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"

import { useCreateUserMutation, useLogInMutation } from "../store/queries/authApi"
import { setBackgroundBlur, setCreateAccountModal, setUser } from "../store/slices/userSlice"

import closeIcon from "../images/close-icon.svg"

export default function CreateUser() {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [login] = useLogInMutation()
    const [createUser] = useCreateUserMutation()

    const initialForm = {
        name: '',
        username: '',
        password: '',
        profile_image: ''
    }

    const [formData, setFormData] = useState(initialForm)

    const handleFormChange = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        const result = await createUser(formData)
        if (result.data){
            const logged = await login(formData)
            if (logged.data) {
                dispatch(setCreateAccountModal(false))
                dispatch(setBackgroundBlur(false))
                dispatch(setUser({
                    name: formData.name,
                    username: formData.username,
                    profile_image: formData.profile_image
                }))
                navigate('/')
            }
        }
    }

    const handleCloseButton = () => {
        dispatch(setCreateAccountModal(false))
        dispatch(setBackgroundBlur(false))
    }
    return (
        <div id="create-account-container">
            <div id="create-account-card">
                <h1>create new user</h1>
                <form id="create-account-form" onSubmit={handleSubmit}>
                    <input className="input" name="name" onChange={handleFormChange} placeholder="Full name" value={formData.name}/>
                    <input className="input" name="profileImage" onChange={handleFormChange} placeholder="profile image url" value={formData.profileImage}/>
                    <input className="input" name="username" onChange={handleFormChange} placeholder="Username" value={formData.username}/>
                    <input className="input" name="password" onChange={handleFormChange} placeholder="Password" value={formData.password}/>
                    <button className="submitButton">submit</button>
                </form>
                <button className="close-button" onClick={handleCloseButton}><img src={closeIcon}/></button>
            </div>
        </div>
    )
}
