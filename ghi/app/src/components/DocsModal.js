import jsonIcon from '../images/json-icon.svg'
import websocketIcon from '../images/websocket-icon.svg'

export default function DocsModal() {

    return (
        <div id="docs-modal">
            <div className="nav-menu-title">Documentation</div>
            <button className="nav-buttons"><img alt='' src={jsonIcon} className='icons'/>- REST API Docs</button>
            <button className="nav-buttons"><img alt='' src={websocketIcon} className='icons'/>- Websocket Docs</button>
        </div>
    )
}
