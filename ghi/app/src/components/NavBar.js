import { useState } from "react"
import { useNavigate } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"

import { useGetTokenQuery, useLogOutMutation } from "../store/queries/authApi"
import { useSymbolLookupMutation } from "../store/queries/finnhubApi"

import userSlice, {
    setCreateAccountModal,
    setLoginModal,
    setBackgroundBlur,
    setSymbolLookUpResult,
    setWindowWidth
} from "../store/slices/userSlice"

import {
    setCompany,
    setDocs,
    setNavModal,
    setServices
} from "../store/slices/navbarSlice"

import LoginModal from './LoginModal';
import CreateUser from './CreateUser';
import ServicesModal from "./ServicesModal"
import DocsModal from "./DocsModal"
import CompanyModal from "./CompanyModal"
import SymbolSearchResult from "./SymbolSearchResult"
import NavMenuModal from "./NavMenuModal"

import expandIcon from '../images/expand-icon.svg'
import compressIcon from '../images/compress-icon.svg'

import {
    menuIcon,
    settingIcon
} from "../images/Icons"

export default function NavBar() {
    // Setup hooks
    const navigate = useNavigate()
    const dispatch = useDispatch()

    const [logout] = useLogOutMutation()
    const [symbolLookup] = useSymbolLookupMutation()

    window.addEventListener('resize', (event) => {
        dispatch(setWindowWidth(event.target.innerWidth))
    })

    // Gather global states
    const userSliceData = useSelector(state => state.user)
    const token = useGetTokenQuery()
    const navbarData = useSelector(state => state.navbar)

    // Setup local hooks
    const [searchForm, setSearchForm] = useState('')

    // Handling authorization events
    const handleLoginButton = () => {
        dispatch(setLoginModal(!userSliceData.loginModal.visible))
        dispatch(setBackgroundBlur(!userSliceData.backgroundBlur.inEffect))
    }
    const handleLogoutButton = () => {
        logout()
        navigate('/')
    }
    const handleSignUpButton = () => {
        dispatch(setCreateAccountModal(true))
        dispatch(setBackgroundBlur(!userSliceData.backgroundBlur.inEffect))
    }

    // Handling symbol lookup queries
    const handleSearch = event => {
        setSearchForm(event.currentTarget.value)
    }
    const handleSearchSubmit = async event => {
        event.preventDefault()
        const result = await symbolLookup(searchForm)
        console.log(result)
        if (result.data){
            dispatch(setSymbolLookUpResult({
                visible: true,
                data: result.data.result
            }))
        } else {
            dispatch(setSymbolLookUpResult({
                visible: true,
                data: ['no data']
            }))
        }
    }

    // Handling navbar menus
    const handleHomeButton = () => {
        dispatch(setServices(false))
        dispatch(setDocs(false))
        dispatch(setCompany(false))
        navigate('/')
    }
    const handleServiceButton = () => {
        dispatch(setServices(!navbarData.services.visible))
        dispatch(setDocs(false))
        dispatch(setCompany(false))
    }
    const handleDocsButton = () => {
        dispatch(setServices(false))
        dispatch(setDocs(!navbarData.docs.visible))
        dispatch(setCompany(false))
    }
    const handleCompanyButton = () => {
        dispatch(setServices(false))
        dispatch(setDocs(false))
        dispatch(setCompany(!navbarData.company.visible))
    }
    const handlePricingButton = () => {
        dispatch(setServices(false))
        dispatch(setDocs(false))
        dispatch(setCompany(false))
        navigate('/pricing')
    }
    const handleContactButton = () => {
        dispatch(setServices(false))
        dispatch(setDocs(false))
        dispatch(setCompany(false))
        navigate('/contact')
    }
    // Handling Nav Collapsed
    const handleNavModal = () => {
        dispatch(setNavModal(!navbarData.navModal.visible))
    }
    return (
        <nav id="navbar">
            {userSliceData.loginModal.visible && <LoginModal/>}
            {userSliceData.createAccountModal.visible && <CreateUser/>}
            {navbarData.services.visible && <ServicesModal/>}
            {navbarData.docs.visible && <DocsModal/>}
            {navbarData.company.visible && <CompanyModal/>}
            {userSliceData.symbolLookUpResult.visible && <SymbolSearchResult/>}
            {navbarData.navModal.visible
            && <NavMenuModal
            handleLogoutButton={handleLogoutButton}
            handleLogin={handleLoginButton}
            handleSignup={handleSignUpButton}
            handlePricingButton={handlePricingButton}
            handleContactButton={handleContactButton}
            />}
            {userSliceData.windowWidth > 1445
            ? (
                <div className="nav-list">
                    <div className="nav-list nav-products">
                        <button onClick={handleHomeButton} className="nav-buttons nav-home"><span>Brokepanda</span><span className="domain-color">.io</span></button>
                        <button onClick={handleServiceButton} className="nav-buttons">
                            Services {navbarData.services.visible
                            ? <img alt='' src={compressIcon} className="icons" />
                            : <img alt='' src={expandIcon} className="icons"/>}
                        </button>
                        <button onClick={handleDocsButton} className="nav-buttons">
                            Docs {navbarData.docs.visible
                            ? <img alt='' src={compressIcon} className="icons"/>
                            : <img alt='' src={expandIcon} className="icons"/>}
                        </button>
                        <button onClick={handleCompanyButton} className="nav-buttons">
                            Company {navbarData.company.visible
                            ? <img alt='' src={compressIcon} className="icons"/>
                            : <img alt='' src={expandIcon} className="icons"/>}
                        </button>
                        <button onClick={handlePricingButton} className="nav-buttons">Pricing</button>
                        <button onClick={handleContactButton} className="nav-buttons">Contact</button>
                    </div>
                    <form onSubmit={handleSearchSubmit} className="nav-search-bar">
                        <input onChange={handleSearch} className="nav-search" placeholder="search"/>
                    </form>
                    <div className="nav-accounts">
                        {token.data
                        ? <button onClick={()=>{navigate('profile')}} className="nav-buttons profile-icon">{settingIcon}</button>
                        : <button onClick={handleLoginButton} className="nav-buttons">Log in</button>
                        }
                        {!token.data && <button className="nav-buttons" onClick={handleSignUpButton}>Sign up</button>}
                        {token.data && <button className="nav-buttons" onClick={handleLogoutButton}>Log out</button>}
                    </div>
                </div>
            )
            : (
                <div className="nav-list">
                    <button onClick={handleHomeButton} className="nav-buttons nav-home">Brokepanda.io</button>
                    <button onClick={handleNavModal} className="nav-buttons">{menuIcon}</button>
                </div>
            )}
        </nav>
    )
}
