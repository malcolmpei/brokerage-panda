import { useState } from "react"

import { useLogInMutation } from "../store/queries/authApi"
import { setBackgroundBlur, setCreateAccountModal, setLoginModal, setUser } from "../store/slices/userSlice"

import { useDispatch, useSelector } from "react-redux"

import closeIcon from "../images/close-icon.svg"

export default function LoginModal() {
    // Setup hooks
    const dispatch = useDispatch()
    const [login] = useLogInMutation()

    // Gather global states
    const userSliceData = useSelector(state => state.user)

    // Setup local hooks
    const [formData, setFormData] = useState({
        username: '',
        password: ''
    })

    // Handling interactive forms and buttons
    const handleFormData = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        const result = await login(formData)
        if (result.data){
            dispatch(setLoginModal(false))
            dispatch(setBackgroundBlur(false))
            dispatch(setUser({
                ...userSliceData.user,
                username:formData.username
            }))
        }
    }

    const handleCreateAccountButton = () => {
        dispatch(setLoginModal(false))
        dispatch(setCreateAccountModal(true))
    }

    const handleCloseButton = () => {
        dispatch(setLoginModal(false))
        dispatch(setBackgroundBlur(false))
    }

    return (
        <div id="loginModalContainer">
            <div id="loginModal">
                <h2>Login</h2>
                <form onSubmit={handleSubmit} id="loginForm">
                    <input className="loginInput" name="username" placeholder="username" onChange={handleFormData}/>
                    <input className="loginInput" name="password" placeholder="password" onChange={handleFormData}/>
                    <button className="submitButton">submit</button>
                </form>
                <button id="create-account-button" onClick={handleCreateAccountButton}>create account</button>
                <button className="close-button" onClick={handleCloseButton}><img alt='' src={closeIcon}/></button>
            </div>
        </div>
    )
}
