import { useGetTokenQuery } from "../store/queries/authApi"
import { useNavigate } from "react-router-dom"

import Footer from "../components/Footer"

export default function ProfilePage(){
    const navigate = useNavigate()
    const token = useGetTokenQuery()
    const handleUpdate = () => {
        navigate('update')
    }
    if (token.currentData) {
        const profileImg = token.currentData.user.profile_image
        return (
            <>
                <div id="profile-page">
                    <div className="profile-icon-name">
                        <div className="profile-image-container">
                            <img className="profile-image" src={profileImg}/>
                        </div>
                        <div className="user-text">
                            <div className="user-name">{token.currentData.user.name}</div>
                            <div className="user-handle">@{token.currentData.user.username}</div>
                            <button onClick={handleUpdate} className="submitButton user-update-button">update</button>
                        </div>
                    </div>
                    <div className="profile-favorites">
                        <div className="profile-title">Your Brokerage</div>
                        <input className="input" placeholder="search"/>
                        <div className="profile-stock-list">
                            <div className="profile-stock-card"></div>
                            <div className="profile-stock-card"></div>
                            <div className="profile-stock-card"></div>
                            <div className="profile-stock-card"></div>
                        </div>
                        <button className="submitButton">see all</button>
                    </div>
                </div>
                <div id="save-stock">
                    <div id="save-stock-content">
                        <div className="profile-title">Add Stock to your Brokerage</div>
                        <div id="save-stock-container">
                            <form className="save-stock-form">
                                <label>Ticker Symbol</label>
                                <input className="input" name="symbol" placeholder="AAPL"/>
                                <label>Name</label>
                                <input className="input" name="name" placeholder="Apple.Inc"/>
                                <label>Logo</label>
                                <input className="input" name="logo" placeholder="https://apple.com/logo"/>
                                <button className="submitButton">save</button>
                            </form>
                            <div className="save-info">Simply add your favorite securities into the database for quick-access.</div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </>
        )
    } else {
        return (
            <>
                <div id="profile-page">
                    <div>profile page</div>
                    <div>not logged in</div>
                    <button>log in</button>
                </div>
                <Footer/>
            </>
        )
    }
}
