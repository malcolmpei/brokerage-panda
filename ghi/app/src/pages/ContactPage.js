import Footer from '../components/Footer'

export default function ContactPage() {

    return (
        <>
            <div id="contact-page">
                <div className="splash-color">Let's talk</div>
                <div className="contact-slogan">Contact Us</div>
                <div>Dedicated people working to provde you with exceptional service</div>
                <button className="submitButton">Reach out</button>
                <div id="contact-email">
                    <div className="contact-email-card">
                        <div className="contact-titles">Connect with email</div>
                        <div>Connect with our team via email for tailored responses</div>
                    </div>
                    <div id="contact-cards">
                        <div className="contact-card">
                            <div className="contact-titles">Sales Team</div>
                        </div>
                        <div className="contact-card">
                            <div className="contact-titles">Support Team</div>
                        </div>
                        <div className="contact-card">
                            <div className="contact-titles">Join Us</div>
                        </div>
                        <div className="contact-card">
                            <div className="contact-titles">Say hello</div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </>
    )
}
