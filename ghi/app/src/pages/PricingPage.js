import backgroundImg from '../images/grid-background.jpg'
import Footer from '../components/Footer'

export default function PricingPage() {
    const prices = [
        {
            title: 'Basic',
            description: 'Easy access for easy use',
            price: 0,
            access: [
                '5 API Calls / Minute',
                'End of Day Data',
                '2 Years Historical Data',
                '100% Market Coverage',
                'Reference Data',
                'Fundamentals Data',
                'Corporate Actions',
                'Aggregates',
                'Technical Indicators'
                ],
            usage: 'Individual Use'
        },
        {
            title: 'Starter',
            description: 'Perfect for aggregates',
            price: 29,
            access: [
                '5 API Calls / Minute',
                'End of Day Data',
                '2 Years Historical Data',
                '100% Market Coverage',
                'Reference Data',
                'Fundamentals Data',
                'Corporate Actions',
                'Aggregates',
                'Technical Indicators',
                'Snapshot',
                'WebSocket'
                ],
            usage: 'Individual Use'
        },
        {
            title: 'Developer',
            description: 'Historical Data for large applications',
            price: 79,
            access: [
                '5 API Calls / Minute',
                'End of Day Data',
                '2 Years Historical Data',
                '100% Market Coverage',
                'Reference Data',
                'Fundamentals Data',
                'Corporate Actions',
                'Aggregates',
                'Technical Indicators',
                'Snapshot',
                'WebSocket',
                'Trades'
                ],
            usage: 'Individual Use'
        }
    ]
    return (
        <>
            <div id="pricing-page">
                <img src={backgroundImg} className="home-bg" alt=''/>
                <div className="splash-color">Access anytime.</div>
                <div className="pricing-slogan">Clear Pricing</div>
                <div>Transparent pricing, easy access, usable market data</div>
                <button className="submitButton">Get Started</button>
                <div id="pricing-container">
                    {prices.map(price => {
                        return (
                            <div key={price.title}>
                                <div className="price-card">
                                    <div className="price-title">{price.title}</div>
                                    <div className="price-description">{price.description}</div>
                                    <div className="price-price">${price.price}<span className="price-price-month">/month</span></div>
                                    <div className="price-access">
                                        {price.access.map(item => {
                                            let key = `${price.title}-${item}`
                                            return (
                                                <div key={key}>- {item}</div>
                                            )
                                        })}
                                    </div>
                                    <div className="price-usage">{price.usage}</div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
            <Footer/>
        </>
    )
}
