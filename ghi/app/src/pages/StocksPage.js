import { useState } from "react"
import { useGetTickerDetailsMutation, useGetTickerListMutation } from "../store/queries/polygonApi"


export default function StocksPage() {
    const [getTickerList] = useGetTickerListMutation()
    const handleGetTickerList = async () => {
        const stocks = await getTickerList(10)
        console.log(stocks)
    }
    const [getTickerDetails] = useGetTickerDetailsMutation()
    const [formData, setFormData] = useState({
        symbol: ''
    })
    const [tickerDetail, setTickerDetail] = useState({
        address: {},
        branding: {},
        description: ''
    })
    const handleGetTickerDetails = async event => {
        event.preventDefault()
        const stockData = await getTickerDetails(formData.symbol)
        console.log(stockData)
        if (stockData.data.active === true) {
            setTickerDetail({
                address: stockData.data.address,
                branding: stockData.data.branding,
                description: stockData.data.description,
                homepageUrl: stockData.data.homepage_url,
                listDate: stockData.data.list_date,
                name: stockData.data.name,
                phoneNum: stockData.data.phone_number,
                marketCap: stockData.data.market_cap
            })
        }
    }
    const handleFormData = event => {
        setFormData({
            ...formData,
            [event.currentTarget.name]:event.currentTarget.value
        })
        console.log(tickerDetail.address)
    }

    return (
        <div id="stocks-page">
            <div className="stock-spotlight">Security Spotlight</div>
            <button onClick={handleGetTickerList}>get stocks</button>
            <div className="stock-search">Search by Symbol</div>
            <form onSubmit={handleGetTickerDetails}>
                <input className="input" onChange={handleFormData} name="symbol" placeholder="AAPL"/>
            </form>
            <div>
                <div className="ticker-name">{tickerDetail.name}</div>
                <div className="ticker-address">{tickerDetail.address.address1}</div>
                <div className="ticker-description">{tickerDetail.description}</div>
            </div>
        </div>
    )
}
