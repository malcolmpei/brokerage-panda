import { useGetMarketNewsQuery } from "../store/queries/finnhubApi"
import Footer from "../components/Footer"


export default function MarketNews() {
    const marketNews = useGetMarketNewsQuery()
    const handleNewsLink = event => {
        let link = event.currentTarget.innerText.split("https://")[1].split("source")[0]
        window.location.href = `https://${link}`
    }
    return (
        <>
            <div id="market-news">
                <div className="news-intro-headline">Market News</div>
                <div id="news-container">
                    {marketNews.isSuccess
                    ? marketNews.currentData.map(article => {
                        let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }
                        let date = new Date(article.datetime)
                        return (
                            <div type="button" onClick={handleNewsLink} className="news-card" key={`marketNews-${article.id}`}>
                                <img alt='' src={article.image} className="news-image"/>
                                <div className="news-text">
                                    <div className="news-headline">{article.headline}</div>
                                    <div className="news-summary">{article.summary}</div>
                                    <div className="news-link">{article.url}</div>
                                    <div className="news-meta">
                                        <div className="news-source"><span className="tag">source: </span>{article.source}</div>
                                        <div className="news-date"><span className="tag">date:</span>{date.toLocaleDateString('en-US', options)}</div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                    : marketNews.isLoading
                    ? <div className="loading-icon"></div>
                    : <div className="error-message">Failed to retrieve data</div>
                    }
                </div>
            </div>
            <Footer/>
        </>
    )
}
