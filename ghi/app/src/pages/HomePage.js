import { useSelector } from "react-redux"

import Footer from '../components/Footer'

import backgroundImg from "../images/grid-background.jpg"

export default function HomePage() {
    const windowWidth = useSelector(state => state.user.windowWidth)
    return (
        <div>
            <div id="homepage">
                <img src={backgroundImg} className="home-bg" alt=''/>
                <div id="home-intro">
                    <button className="splash-button">What's new</button>
                    <button className="home-intro-buttons">Release notes</button>
                </div>
                <div className={windowWidth > 1445 ? "home-slogan" : "home-slogan slogan-md"}>Designing <span className="splash-color">Brokerages</span> with the latest financial data.</div>
                <div className="home-pitch">At Brokepanda.io, we are on a mission to facilitate the access of financial tools to the average person with clean and intuative design.</div>
                <div id="home-docs">
                    <button className="splash-button">API Docs</button>
                </div>
                <div id="home-sponsers">Powered by</div>
                <br></br>
                <div id="home-logos">
                    <div className="logo-placeholder"></div>
                    <div className="logo-placeholder"></div>
                    <div className="logo-placeholder"></div>
                    <div className="logo-placeholder"></div>
                </div>
            </div>
            <div id="home-howto">
                <div id="home-howto-container">
                    <h1 className="home-slogan">How to use</h1>
                    <div className="home-pitch">Simply create an account and get started by accessing the varied products in our line up</div>
                </div>
            </div>
            <Footer/>
        </div>
    )
}
