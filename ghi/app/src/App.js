import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'

import NavBar from './components/NavBar';

import HomePage from './pages/HomePage';
import ProfilePage from './pages/ProfilePage';
import UpdateUser from './components/UpdateUser';
import MarketNews from './pages/MarketNews';
import SymbolQuotePage from './pages/SymbolQuotePage';
import StocksPage from './pages/StocksPage';
import PricingPage from './pages/PricingPage';
import ContactPage from './pages/ContactPage';

import { useSelector } from 'react-redux';

function App() {
  const userData = useSelector(state => state.user)
  return (
    <BrowserRouter>
      <div className='App'>
        <NavBar/>
        {userData.backgroundBlur.inEffect && <div id="background-blur"></div>}
        <Routes>
          <Route path="/" element={<HomePage/>}/>
          <Route path="/pricing" element={<PricingPage/>}/>
          <Route path="/contact" element={<ContactPage/>}/>
          <Route path="/profile">
            <Route path="" element={<ProfilePage/>}/>
            <Route path="update" element={<UpdateUser/>}/>
          </Route>
          <Route path="/finnhub">
            <Route path="news" element={<MarketNews/>}/>
            <Route path="quote" element={<SymbolQuotePage/>}/>
          </Route>
          <Route path="/polygon">
            <Route path="stocks" element={<StocksPage/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
