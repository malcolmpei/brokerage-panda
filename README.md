# Brokerage Panda



## Getting started

- Visit [Finnhub](https://finnhub.io/) and request a free API key
- Create a new directory in the api folder named [keys]
- Within the keys directory create a new python file named [finnhub_key.py]
- In the [finnhub_key.py] file add a new variable named [finnhub_key] and paste your secure API key there for usage in the application

If you plan on using docker for your environment. You will need to create a volume named [brokerage-data]
- You can now use `docker compose up --build` to begin using the application

## Project Status
Early stage development. Working on the back-end.
