import os
from fastapi import FastAPI, Depends
from fastapi.middleware.cors import CORSMiddleware

from authenticator import authenticator
from routers import users, finnhub, brokerages, benzinga, polygon


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/", tags=['Introduction'])
def read_root():
    return {"Hello": "World!"}

# Application routes
app.include_router(authenticator.router, tags=['login / logout'])
app.include_router(users.router, tags=['Users'])
app.include_router(finnhub.router, tags=['Finnhub'])
app.include_router(brokerages.router, tags=['Brokerage'])
app.include_router(benzinga.router, tags=['Benzinga'])
app.include_router(polygon.router, tags=['Polygon'])
