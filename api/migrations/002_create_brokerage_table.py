steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE brokerages (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
            symbol VARCHAR(50) NOT NULL,
            name VARCHAR(50),
            logo VARCHAR(100)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE brokerages;
        """
    ]
]
