from jwtdown_fastapi.authentication import Token

from pydantic import BaseModel
from typing import Optional

class UserInWithoutPassword(BaseModel):
    name: str
    username: str
    profile_image: Optional[str]

class UserIn(BaseModel):
    name: str
    username: str
    password: str
    profile_image: Optional[str]


class UserOut(BaseModel):
    id: int
    name: str
    username: str
    profile_image: str

class UserOutWithPassword(BaseModel):
    id: int
    name: str
    username: str
    password: str
    profile_image: str

class UserForm(BaseModel):
    username: str
    password: str

class UserToken(Token):
    user: UserOutWithPassword
