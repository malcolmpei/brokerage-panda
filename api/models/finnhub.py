from pydantic import BaseModel
from typing import List, Optional

class SymbolSearchIn(BaseModel):
    query: str

class SymbolSearchOut(BaseModel):
    description: str
    displaySymbol: str
    symbol: str
    type: str

class SymbolLookUpOut(BaseModel):
    count: int
    result: List[SymbolSearchOut]

class QuoteDetails(BaseModel):
    currentPrice: float
    change: float
    percentChange: float
    highPrice: float
    lowPrice: float
    openPrice: float
    previousClose: float
    t: int

class MarketNewsOut(BaseModel):
    category: str
    datetime: int
    headline: str
    id: int
    image: str
    related: str
    source: str
    summary: str
    url: str

class IpoCalendarDetails(BaseModel):
    date: str | None
    exchange: str | None
    name: str | None
    numberOfShares: int | None
    price: str | None
    status: str | None
    symbol: str | None
    totalSharesValue: int | None

class IpoCalendarOut(BaseModel):
    ipoCalendar: List[IpoCalendarDetails]
