from pydantic import BaseModel
from typing import List, Dict

class StockIn(BaseModel):
    symbol: str
    name: str
    logo: str

class StockOut(BaseModel):
    id: int
    user_id: int
    symbol: str
    name: str
    logo: str
