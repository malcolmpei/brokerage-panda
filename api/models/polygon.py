from pydantic import BaseModel
from typing import Optional

class AggregateOut(BaseModel):
    open: float | None
    hight: int | None
    low: float | None
    close: int | None
    volume: int | None
    vwap: float | None
    timestamp: int | None
    transactions: int | None
    otc: bool | None

class GroupedAggregateOut(BaseModel):
    ticker: str | None
    open: float | None
    high: float | None
    low: float | None
    close: float | None
    volume: int | None
    vwap: float | None
    timestamp: int | None
    transactions: int | None
    otc: bool | None

class OpenCloseAggregateOut(BaseModel):
    after_hours: float | None
    close: float | None
    from_: str | None
    high: float | None
    low: float | None
    open: float | None
    pre_market: float | None
    status: str | None
    symbol: str | None
    volume: int | None
    otc: bool | None

class PreviousAggregateOut(BaseModel):
    ticker: str | None
    close: float | None
    high: float | None
    low: float | None
    open: float | None
    timestamp: int | None
    volume: int | None
    vwap: float | None

class TickerAddress(BaseModel):
    address1: str | None
    address2: str | None
    city: str | None
    state: str | None
    country: str | None
    postal_code: str | None

class TickerBranding(BaseModel):
    icon_url: str | None
    logo_url: str | None
    accent_color: str | None
    light_color: str | None
    dark_color: str | None

class TickerDetailsOut(BaseModel):
    active: bool | None
    address: TickerAddress | None
    branding: TickerBranding | None
    cik: str | None
    composite_figi: str | None
    currency_name: str | None
    currency_symbol: str | None
    base_currency_name: str | None
    base_currency_symbol: str | None
    delisted_utc: str | None
    description: str | None
    ticker_root: str | None
    ticker_suffix: str | None
    homepage_url: str | None
    list_date: str | None
    locale: str | None
    market: str | None
    market_cap: int | None
    name: str | None
    phone_number: str | None
    primary_exchange: str | None
    share_class_figi: str | None
    share_class_shares_outstanding: int | None
    sic_code: str | None
    sic_description: str | None
    ticker: str | None
    total_employees: int | None
    type: str | None
    weighted_shares_outstanding: int | None
