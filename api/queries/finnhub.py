from keys.finnhub_key import finnhub_key
import finnhub

api = finnhub.Client(api_key=finnhub_key)

class finnhubApiQueries:
    def get_symbols(self, query:str):
        symbols = api.symbol_lookup(query)
        return symbols

    def get_quotes(self, symbol:str):
        quote = api.quote(symbol)
        return quote

    def get_market_news(self):
        market_news = api.general_news('general', min_id=0)
        return market_news

    def get_stock_ipo_calendar(self, start, end):
        calendar = api.ipo_calendar(_from=start, to=end)
        return calendar
