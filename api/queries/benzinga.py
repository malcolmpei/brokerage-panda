from benzinga import financial_data
from keys.benzinga_key import benzinga_key

fin = financial_data.Benzinga(benzinga_key)


class BenzingaQueries:
    def get_stock_ratings(self):
        stock_ratings = fin.ratings()
        return stock_ratings
