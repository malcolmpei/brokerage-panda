from psycopg_pool import ConnectionPool
import os

from models.brokerages import StockIn, StockOut
from typing import List

# Connect to the database
pool = ConnectionPool(conninfo=os.environ.get('DATABASE_URL'))

class BrokerageQueries:
    def get_stock_by_id(self, stock_id: int) -> StockOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM brokerages
                        WHERE id=%s;
                        """,
                        [stock_id]
                    )
                    record = result.fetchone()
                    return StockOut(
                        id=record[0],
                        user_id=record[1],
                        symbol=record[2],
                        name=record[3],
                        logo=record[4]
                    )
        except Exception as e:
            print(e)
            return {"exception":str(e)}

    def get_all_stocks(self):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM brokerages
                        ORDER BY id;
                        """
                    )
                    records = result.fetchall()
                    formatted_stocks = []
                    for record in records:
                        stock = StockOut(
                            id=record[0],
                            user_id=record[1],
                            symbol=record[2],
                            name=record[3],
                            logo=record[4]
                        )
                        formatted_stocks.append(stock)
                    return formatted_stocks
        except Exception as e:
            print(e)
            return {"exception": str(e)}

    def create_stock(self, stock: StockIn, user_id: int) -> StockOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO brokerages
                            (symbol, name, logo, user_id)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [stock.symbol, stock.name, stock.logo, user_id]
                    )
                    id = result.fetchone()[0]
                    return StockOut(
                        id=id,
                        user_id=user_id,
                        name=stock.name,
                        symbol=stock.symbol,
                        logo=stock.logo
                    )
        except Exception as e:
            print(e)
            return {"exception": str(e)}

    def update_stock(self, stock: StockIn, stock_id: int) -> StockOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE brokerages
                        SET name=%s, symbol=%s, logo=%s
                        WHERE id=%s
                        RETURNING id, symbol, name, logo, user_id
                        """,
                        [stock.name, stock.symbol, stock.logo, stock_id]
                    )
                    record = result.fetchone()
                    return StockOut(
                        id=record[0],
                        symbol=record[1],
                        name=record[2],
                        logo=record[3],
                        user_id=record[4]
                    )
        except Exception as e:
            print(e)
            return {"exeception": str(e)}

    def delete_stock(self, stock_id:int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        DELETE FROM brokerages
                        WHERE id = %s;
                        """,
                        [stock_id]
                    )
                    return True if result is not None else False
        except Exception as e:
            print(e)
            return {"exception": str(e)}
