from keys.polygon_key import polygon_key
from polygon import RESTClient

from datetime import datetime, timedelta

client = RESTClient(api_key=polygon_key)

today = datetime.now().strftime('%Y-%m-%d')
yesterday = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')
month_ago = (datetime.now() - timedelta(days=31)).strftime('%Y-%m-%d')


class polygonQueries:
    def list_tickers(self, limit:int):
        try:
            tickers = client.list_tickers(limit=limit)
            return tickers
        except Exception as e:
            print(e)
            return {"message":str(e)}

    def get_ticker_details(self, symbol:str):
        try:
            ticker = client.get_ticker_details(ticker=symbol)
            return ticker
        except Exception as e:
            print(e)
            return {"exception": str(e)}

    def get_aggregate(self, symbol:str):
        try:
            aggregate = client.get_aggs(
                ticker=symbol,
                multiplier=1,
                timespan="minute",
                from_="2023-01-01",
                to="2023-06-13",
            )
            return aggregate
        except Exception as e:
            print(e)
            return {"exception": str(e)}

    def get_aggregates(self, symbol:str):
        try:
            aggregates = []
            for aggregate in client.list_aggs(
                ticker=symbol,
                multiplier=1,
                timespan="minute",
                from_="2023-01-01",
                to="2023-06-13",
                limit=50000
                ):
                aggregates.append(aggregate)
            return aggregates
        except Exception as e:
            print(e)
            return {"exception": str(e)}

    def get_grouped_daily_aggregates(self):
        try:
            aggregates = []
            for aggregate in client.get_grouped_daily_aggs(
                date=month_ago,
                locale="us",
                market_type="stocks",
                include_otc=True,
            ):
                aggregates.append(aggregate)
            return aggregates
        except Exception as e:
            print(e)
            return {"exception": str(e)}

    def get_daily_open_close_aggregate(self, symbol:str):
        try:
            aggregate = client.get_daily_open_close_agg(
                ticker=symbol,
                date=month_ago,
            )
            return aggregate
        except Exception as e:
            print(e)
            return {"exception": str(e)}

    def get_previous_close_aggregate(self, symbol:str):
        try:
            aggregate = client.get_previous_close_agg(
                ticker=symbol
            )
            return aggregate
        except Exception as e:
            print(e)
            return {"exception": str(e)}

    def get_last_trade(self, symbol:str):
        try:
            trades = client.list_trades(ticker=symbol, timestamp=today)
            return trades
        except Exception as e:
            print(e)
            return {"exception": str(e)}
