from psycopg_pool import ConnectionPool
import os

from typing import List
from models.users import UserIn, UserOut, UserOutWithPassword

# Connect to the database
pool = ConnectionPool(conninfo=os.environ.get('DATABASE_URL'))

class UserQueries:
    def create_user(self, user: UserIn, hashed_password: str):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO users
                            (name, username, password, profile_image)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [user.name, user.username, hashed_password, user.profile_image]
                    )
                    id = result.fetchone()[0]
                    return UserOutWithPassword(
                        id = id,
                        username = user.username,
                        password = hashed_password,
                        name = user.name,
                        profile_image = user.profile_image
                    )
        except Exception as e:
            print(e)
            return {"exception": e}

    def delete_user(self, user_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        DELETE FROM users
                        WHERE id = %s;
                        """,
                        [user_id]
                    )
                    return True if result is not None else False
        except Exception as e:
            print(e)
            return {"exception": e}

    def update_user(self, user: UserIn, user_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        UPDATE users
                        SET name=%s, username=%s, profile_image=%s
                        WHERE id = %s
                        RETURNING password;
                        """,
                        [user.name, user.username, user.profile_image, user_id]
                    )
                    password = result.fetchone()[0]
                    return UserOutWithPassword(
                        id=user_id,
                        username=user.username,
                        password=password,
                        name=user.name,
                        profile_image=user.profile_image
                    )
        except Exception as e:
            print(e)
            return {"exception": e}

    def get_user_by_username(self, username: str) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT name, username, password, profile_image, id
                        FROM users
                        WHERE username = %s;
                        """,
                        [username]
                    )
                    record = result.fetchone()
                    if record is None:
                        return {"message":"could not get user"}
                    return UserOutWithPassword(
                        name = record[0],
                        username = record[1],
                        password = record[2],
                        profile_image = record[3],
                        id = record[4]
                    )
        except Exception as e:
            print(e)
            return {"exception":e}

    def list_all_users(self) -> List[UserOutWithPassword]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        SELECT *
                        FROM users
                        """
                    )
                    records = result.fetchall()
                    if records is None:
                        return {"message":"count not list users"}

                    formatted_data = []
                    for record in records:
                        user = UserOutWithPassword(
                            id = record[0],
                            name = record[1],
                            username = record[2],
                            password = record[3],
                            profile_image = record[4]
                        )
                        formatted_data.append(user)
                    return formatted_data
        except Exception as e:
            print(e)
            return {"exception": e}
