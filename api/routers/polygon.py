from fastapi import APIRouter, Depends, Query

from queries.polygon import polygonQueries
from models.polygon import (
    AggregateOut,
    GroupedAggregateOut,
    OpenCloseAggregateOut,
    PreviousAggregateOut,
    TickerDetailsOut
    )

from typing import List

router = APIRouter()


@router.get('/api/polygon/tickers')
def list_tickers(
    limit: int,
    repo: polygonQueries = Depends()
):
    result = repo.list_tickers(limit)
    return result

@router.get('/api/polygon/ticker', response_model=TickerDetailsOut | dict)
def get_ticker_details(
    symbol: str,
    repo: polygonQueries = Depends()
):
    result = repo.get_ticker_details(symbol)
    return result

@router.get('/api/polygon/aggregate', response_model=List[AggregateOut])
def get_aggregate(
    symbol:str,
    repo:polygonQueries = Depends()
):
    result = repo.get_aggregate(symbol)
    return result

@router.get('/api/polygon/aggregates', response_model=List[AggregateOut])
def get_aggregates(
    symbol: str,
    repo: polygonQueries = Depends()
):
    result = repo.get_aggregates(symbol)
    return result

@router.get('/api/polygon/grouped/daily/aggregates', response_model=List[GroupedAggregateOut] | dict)
def get_grouped_daily_aggregates(
    repo: polygonQueries = Depends()
):
    result = repo.get_grouped_daily_aggregates()
    return result

@router.get('/api/polygon/daily/open/close/aggregate', response_model=OpenCloseAggregateOut | dict)
def get_daily_open_close_aggregate(
    symbol:str,
    repo: polygonQueries = Depends()
):
    result = repo.get_daily_open_close_aggregate(symbol)
    return result

@router.get('/api/polygon/previous/close/aggregate', response_model=PreviousAggregateOut | dict)
def get_previous_close_aggregate(
    symbol: str,
    repo: polygonQueries = Depends()
):
    result = repo.get_previous_close_aggregate(symbol)
    return result

@router.get('/api/polygon/trade')
def get_last_trade(
    symbol:str,
    repo: polygonQueries = Depends()
):
    result = repo.get_last_trade(symbol)
    return result
