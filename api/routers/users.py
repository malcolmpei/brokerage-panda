from fastapi import APIRouter, Depends, Request, Response, HTTPException, status
from authenticator import authenticator
from models.users import UserIn, UserInWithoutPassword, UserOutWithPassword, UserForm, UserToken
from queries.users import UserQueries

from typing import Optional, List

router = APIRouter()

class DuplicateUserError(ValueError):
    pass

@router.get('/api/user', response_model=UserOutWithPassword)
def get_user_by_username(
    username: str,
    repo: UserQueries = Depends()
):
    return repo.get_user_by_username(username)

@router.post('/api/user', response_model=UserToken)
async def create_user(
    user: UserIn,
    request: Request,
    response: Response,
    repo: UserQueries = Depends()
):
    hashed_password = authenticator.hash_password(user.password)
    try:
        result = repo.create_user(user, hashed_password)
        form = UserForm(username=user.username, password=user.password)
        token = await authenticator.login(response, request, form, repo)
        return UserToken(user=result, **token.dict())
    except DuplicateUserError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials"
        )

@router.delete('/api/user')
async def delete_user(
    repo: UserQueries = Depends(),
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data)
):
    if account_data:
        return repo.delete_user(account_data['id'])

@router.put('/api/user', response_model=UserOutWithPassword | dict)
async def update_user(
    user: UserInWithoutPassword,
    repo: UserQueries = Depends(),
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data)
):
    if account_data:
        try:
            result = repo.update_user(user, account_data['id'])
            return result
        except DuplicateUserError:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Cannot update account with these credentials"
            )
    else:
        return {"message":"not logged in"}

@router.get('/api/users', response_model=List[UserOutWithPassword])
def list_all_users(
    repo: UserQueries = Depends()
):
    return repo.list_all_users()

@router.get('/token', response_model=UserToken | None)
async def get_token(
    request: Request,
    repo: UserQueries = Depends(authenticator.try_get_current_account_data)
):
    if repo and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "user": repo
        }
