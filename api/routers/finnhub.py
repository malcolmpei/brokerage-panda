from fastapi import APIRouter, Depends, Query

from queries.finnhub import finnhubApiQueries
from models.finnhub import SymbolLookUpOut, QuoteDetails, MarketNewsOut, IpoCalendarOut

from typing import List, Optional, Annotated

import datetime

router = APIRouter()

@router.get('/api/finnhub/symbols', response_model=SymbolLookUpOut)
def symbol_lookup(
        query: str,
        repo: finnhubApiQueries = Depends()
):
    result = repo.get_symbols(query)
    return SymbolLookUpOut(
        count = result['count'],
        result = result['result']
    )

@router.get('/api/finnhub/quote', response_model=QuoteDetails)
def get_quote(
    symbol: str,
    repo: finnhubApiQueries = Depends()
):
    result = repo.get_quotes(symbol)
    return QuoteDetails(
        currentPrice=result['c'],
        change=result['d'],
        percentChange=result['dp'],
        highPrice=result['h'],
        lowPrice=result['l'],
        openPrice=result['o'],
        previousClose=result['pc'],
        t=result['t']
    )

@router.get('/api/finnhub/news', response_model=List[MarketNewsOut])
def get_market_news(
    repo: finnhubApiQueries = Depends()
):
    result = repo.get_market_news()
    return result

@router.post('/api/finnhub/ipo/calendar', response_model=IpoCalendarOut)
def get_ipo_calendar(
    start: str = Query(description='yyyy-mm-dd'),
    end: str = Query(description='yyy-mm-dd'),
    repo: finnhubApiQueries = Depends()
):

    month_ago = (datetime.datetime.now() - datetime.timedelta(days=31)).strftime('%Y-%m-%d')
    month_later = (datetime.datetime.now() + datetime.timedelta(days=31)).strftime('%Y-%m-%d')

    result = repo.get_stock_ipo_calendar(start, end)
    return result
