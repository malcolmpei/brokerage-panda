from fastapi import APIRouter, Depends

from queries.benzinga import BenzingaQueries

router = APIRouter()

@router.get('/api/benzinga/stock-ratings')
def get_stock_ratings(
    repo: BenzingaQueries = Depends()
):
    return repo.get_stock_ratings()
