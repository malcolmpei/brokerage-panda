from fastapi import APIRouter, Depends
from authenticator import authenticator

from models.brokerages import StockIn, StockOut
from queries.brokerages import BrokerageQueries

from typing import Optional, List

router = APIRouter()

@router.get('/api/stocks', response_model=List[StockOut])
def get_all_stocks(
    repo: BrokerageQueries = Depends()
):
    return repo.get_all_stocks()

@router.get('/api/stock', response_model=StockOut)
def get_stock_by_id(
    stock_id: int,
    repo: BrokerageQueries = Depends()
):
    return repo.get_stock_by_id(stock_id)

@router.post('/api/stocks', response_model=StockOut)
def create_stock(
    stock: StockIn,
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data),
    repo: BrokerageQueries = Depends()
):
    result = repo.create_stock(stock, account_data['id'])
    return result

@router.put('/api/stocks', response_model=StockOut | dict)
def update_stock(
    stockUpdate: StockIn,
    stock_id: int,
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data),
    repo: BrokerageQueries = Depends()
):
    if account_data:
        stock = repo.get_stock_by_id(stock_id)
        if stock.user_id == account_data['id']:
            result = repo.update_stock(stockUpdate, stock_id)
            return result
        else:
            return {"message":"wrong user"}
    else:
        return {"message":"not logged in"}

@router.delete('/api/stocks', response_model=bool | dict)
def delete_stock(
    stock_id: int,
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data),
    repo: BrokerageQueries = Depends()
):
    if account_data:
        stock = repo.get_stock_by_id(stock_id)
        if stock.user_id == account_data['id']:
            result = repo.delete_stock(stock_id)
            return result
        else:
            return {"message":"wrong user"}
    else:
        return {"message":"not logged in"}
